#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define LENGTH 80

int main()
{
    printf("vvedite stroku\n");
    char string[LENGTH];
    gets(string);

    for (int i = 0; string[i] != '\0'; ++i) {
        if(ispunct(string[i])) string[i] = ' ';
    }

    int symbol = 0;
    char space[] = " ";
    char *row = strtok(string, space);
    char word[LENGTH] = "";

    while (row != '\0') {
        if (strlen(row) > symbol) {
            symbol = strlen(row);
            strcpy(word, row);
        }
        row = strtok('\0',space);
    }
    printf("The longest word is \"%s\" it has %d symbols\n", word, symbol);

    return 0;
}