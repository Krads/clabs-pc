#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define AMOUNT 20
#define RANGE 100

int main()
{
    int string[AMOUNT] = {0};
    srand(time(0));
    int lim = 0;

    if (AMOUNT % 2 == 1)
        lim = AMOUNT/2 + 1;
    else
        lim = AMOUNT/2;

    int count_i = 0, count_s = 0;
    int positive = 0, negative = 0;
    int sum = 0;

    for ( int i = 0; i < AMOUNT; i++) {
        switch (rand() % 2 + 1) {
            case 1:
                count_i++;
                positive = i;
                string[i] = rand() % RANGE + 1;
                break;
            case 2:
                count_s++;
                if (count_s == 1) negative = i;
                string[i] = rand() % RANGE - RANGE;
                break;
        }
        printf("%d\n", string[i]);
    }

    for (; negative <= positive; negative++)
        sum += string[negative];

    printf("sum is: %d\n", sum);

    return 0;
}
