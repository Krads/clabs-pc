#include <stdio.h>
#include <string.h>

#define LENGTH 81

int main()
{
    printf("Vvedite polindrom\n");
    char string[LENGTH];
    gets(string);

    int start = 0, end = strlen(string) - 1;

    for(; start < end; ++start, --end) {
        if (string[start] == string[end]) {
            continue;
        } else {
            break;
        }
    }
    if (start > end)
        printf("stroka palindrom!\n");
    else
        printf("stroka ne palindrom!\n");

    return 0;
}