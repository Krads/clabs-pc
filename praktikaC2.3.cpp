// praktikaC2.3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#define STRING_LENGTH 81

int main()
{
    printf("Vvedite kol-vo strok:\n");
    int stroki = 0;
    scanf("%d", &stroki);
    fflush(stdin);

    if (stroki <= 0 || stroki > 40) {                   // �������� �� ��������� �������� � ����������� ������ ������
        printf("ERROR!\n");
        return 0;

    } else {

        for(int i = 0; i < stroki; i++) {               // ���� ��������
            char star[STRING_LENGTH] = "*";
            memset (star, '*', 2*i+1);
            printf("%*s\n", STRING_LENGTH / 2+i, star);
        }

    }

    return 0;
}

