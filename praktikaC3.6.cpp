#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define AMOUNT 10
#define RANGE 100

int main()
{
    int string[AMOUNT] = {0};
    srand(time(0));
    int lim = 0;

    if (AMOUNT % 2 == 1)
        lim = AMOUNT/2 + 1;
    else
        lim = AMOUNT/2;

    int count_i = 0, count_s = 0;
    int positive = 0, negative = 0;
    int highest_i = 0, lowest_i = 0;
    int sum = 0;

    for ( int i = 0; i < AMOUNT; i++) {
        switch (rand() % 2 + 1) {
            case 1:
                count_i++;
                string[i] = rand() % RANGE + 1;
                if (string[i] > highest_i) {
                    positive = i;
                    highest_i = string[i];
                }
                break;
            case 2:
                count_s++;
                string[i] = rand() % RANGE - RANGE;
                if (string[i] < lowest_i) {
                    negative = i;
                    lowest_i = string[i];
                }
                break;
        }
        printf("%d\n", string[i]);
    }

    if (negative < positive) {
        for (; negative <= positive; negative++)
            sum += string[negative];
    } else {
        for (; positive <= negative; positive++)
        sum += string[positive];
    }

    printf("sum is: %d\n", sum);

    return 0;
}
