// praktikaš5.cpp : Defines the entry point for the console application.
//

#include <stdafx.h>
#include <string.h>
#define LENGTH 81
#define RAZMER_BUFERA 40
int main()
{
    char stroka[LENGTH], format[RAZMER_BUFERA];
    printf("Vvedite stroky:\n");
    scanf("%80s", stroka);
    fflush(stdin);
    sprintf(format, "%%%ds\n", (LENGTH - strlen(stroka)) / 2 + strlen(stroka));
    printf(format, stroka);
    return 0;
}
