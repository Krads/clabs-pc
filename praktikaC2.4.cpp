#include <stdio.h>
#include <string.h>
#define LENGTH 80
int main()
{
    printf("Vvedite stroku:\n");
    char string[LENGTH];
    scanf("%s", string);
    int string_length = strlen(string);

    for (int count = 0; count < string_length; ++count) {

        if (string[count] >= 'a' && string[count] <= 'z') {

            for (int cifra = 0; cifra <= count; ++cifra) {

                if (string[cifra] >= '0' && string[cifra] <= '9') {
                   char save = string[count];
                   string[count] = string[cifra];
                   string[cifra] = save;
                }
            }
        }
    }

    printf("%s\n", string);
    return 0;
}