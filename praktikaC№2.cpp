// praktikaC╣2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main()
{
    int hours, minutes, seconds;
    printf("Enter time HH:MM:SS\n");
    scanf("%d:%d:%d", &hours, &minutes, &seconds);
    if ((hours < 0 || hours > 24) || (minutes < 0 || minutes > 60) || (seconds < 0 || seconds > 60)) {
    printf("wrong format\n");
    return 0;
    }
    printf("The time is %d:%d:%d\n", hours, minutes, seconds);
    if (hours >= 0 && hours <= 12)
        printf("good morning\n");
    else if (hours >= 12 && hours <= 16)
        printf("good day\n");
    else if (hours >= 16 && hours <= 21)
        printf("good evening\n");
    else
        printf("good night\n");
    return 0;
}