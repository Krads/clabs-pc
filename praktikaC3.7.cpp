#include <stdio.h>
#include <string.h>

#define LENGTH 80
#define ASCII 256

int main()
{
    printf("Vvedite stroku\n");
    char string[LENGTH];
    gets(string);
    int amount[ASCII] = {0};

    for (int i = 0; string[i] != '\0'; ++i) amount[string[i]]++;

    for (int k = 0; k < ASCII; ++k) {
        int count_i = 0;
        char count_ch;

        for (int i = k; i < ASCII; ++i) {

            if (i == ' ') amount[i] = 0;

            if (amount[i] > count_i) {
                count_i = amount[i];
                count_ch = i;
            }
        }

        if (amount[count_ch] != 0) {
            printf("%c - %d\n", count_ch, count_i);
            amount[count_ch] = 0;
            k = 0;
        }
    }

    return 0;
}