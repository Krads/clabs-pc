#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#define LENGTH 81

void printWord (char *word);
int getWords (char string[], char* pointer[]);
int randWord ( char *word[], int count);

int main()
{
    srand(time(0));
    printf("Vvedite stroku:\n");
    char string[LENGTH];
    char *words[LENGTH];
    *words = string;
    gets(string);
    int count = getWords(string, words);
    int countword = 0;

    while (countword < count) {
        int k = randWord(words, count);
        if (k < 0) {
            continue;
        } else {
            if (countword != 0) {
                putchar(' ');
            }
            countword++;
            
            for (int i = 0; words[k][i] != NULL && words[k][i] != ' '; ++i)
                putchar(words[k][i]);
            
            words[k] = NULL;
        }
    }
    putchar('\n');

    return 0;
}

int getWords (char string[], char *pointer[])
{
    int count = 0;

    for (int i = 0; string[i] != '\0'; ++i) {
        if (isalpha(string[i])) {
            pointer[count] = &string[i];
            count++;
            while(isalpha(string[i+1]))
                i++;
        }
    }

    return count;
}

int randWord ( char *word[], int count)
{
    int i = rand() % count;
    if (word[i] == NULL)
        return i = -1;
    else
        return i;
}