// praktikaš3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "math.h"
#define pi acos(-1.0)

int main()
{
    float angle;
    char rilid;
    double result;
    printf("Enter angle in rad(R) or deg(D)\n");
    scanf("%f %c", &angle, &rilid);
    if (rilid != 'R' && rilid != 'D' && rilid != 'r' && rilid != 'd' || angle > 360) {
        printf("wrong format\n");
        return 0;
    }
    if (rilid == 'r' || rilid == 'R')
        result = angle * (180 / pi);
    else
        result = (angle * pi) / 180;
    printf("result is %.1f\n", result);
    return 0;
}
