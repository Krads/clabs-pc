#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define PASSWORD 9
#define AMMOUNT 10

int main()
{
    srand(time(0));
    char random[AMMOUNT][PASSWORD];


    for (int i = 0; i < AMMOUNT; ++i) {
        int count_a = 0, count_b = 0, count_c = 0;

        for ( int k = 0; k < PASSWORD-1; ++k) {
            switch (rand() % 3) {
                case 0:
                    random[i][k] = rand() % 25 + 'a';
                    count_a++;
                    break;
                case 1:
                    random[i][k] = rand() % 25 + 'A';
                    count_b++;
                    break;
                case 2:
                    random[i][k] = rand() % 9 + '0';
                    count_c++;
                    break;
            }
        }
        random[i][8] = '\0';
        if (count_a == 0 || count_b == 0 || count_c == 0){
        i--;
        continue;
        }

        printf("%s\n", random[i]);
    }

    return 0;
}