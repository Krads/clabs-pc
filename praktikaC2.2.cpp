// praktikaC2.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#define DIAPAZON 100

int main()
{
    printf("Enter number:\n");
    srand(time(0));
    int random_i = rand() % DIAPAZON + 1;
    for (int user_i = 0; user_i != random_i;) {

        if (scanf("%d", &user_i)) {

            if (user_i < random_i)
                printf("more\n");
            else if (user_i > random_i)
                printf("less\n");
            else printf("You win!\n");

        } else {
            printf("ERROR!\n");
            return 0;
        }
    }

    return 0;
}

