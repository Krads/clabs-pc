#include <stdio.h>
#include <string.h>
#define LENGTH 80
#define ASCII 256

int main()
{
    printf("Vvedite stroku\n");
    char string[LENGTH];
    gets(string);
    int amount[ASCII] = {0};

    for (int i = 0; string[i]; ++i) amount[string[i]]++;

    for (int i = 0; i < ASCII; ++i) {
        if (amount[i] && i != ' ') {
            printf("%c - %d\n", i, amount[i]);
        }
    }

    return 0;
}