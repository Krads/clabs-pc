#include <stdio.h>

#define LENGTH 81

int main()
{
    printf("Vvedite stroku simvolov\n");
    char string[LENGTH];
    gets(string);
    char *pointer;
    pointer = string;

    char *start, *end, *symbol;
    start = end = pointer;

    while (*pointer != '\0') {
        symbol = pointer;
        ++pointer;
        while (*pointer == *symbol)
            ++pointer;
        if (pointer - symbol > end - start) {
            end = pointer;
            start = symbol;
        }
    }

    printf("kol-vo simvolov = %d ", end-start);

    for (; end > start; ++start)
        putchar(*start);

    putchar('\n');

    return 0;
}