// praktikaC2.1.cpp : Defines the entry point for the console application.
//

#include <stdafx.h>
#define g 9.81

int main()
{
    printf("vvedite visoty:\n");
    float high;
    scanf("%f", &high);

    if (high == 0) {
        printf("BABAH!\n");
        return 0;
    }

    int time = 0;

    for (double uskorenie = 0; uskorenie >= 0; time++) {
      uskorenie = g * time*time / 2;
      uskorenie = high - uskorenie;
      if (uskorenie < 0) printf("BABAH!\n");
      else printf("t = %02d h = %.1f\n", time, uskorenie);
    }
    return 0;
}

