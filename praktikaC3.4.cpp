#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define LENGTH 81
#define RANK 5

int main()
{
    printf("Enter numbers\n");
    char string[LENGTH];
    gets(string);
    int sum = 0;

    for (int k = 0; k < strlen(string); k++) {
        char str[LENGTH];
        char *end;
        int m = 0;
        strcpy(str,&string[k]);
        if (isdigit(string[k]) != NULL) {

            for (; m < RANK && isdigit(str[m]) != NULL; m++);

            str[m] = '\0';
            int i = strtol(str, &end, 10);
            sum += i;
            k += m-1;
        }
    }
    printf("sum is: %d\n", sum);
    return 0;
}