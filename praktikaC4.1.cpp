#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LENGTH 81
#define RANGE 100

int main()
{
    printf("Vvedite neskolko strok\n");
    char strings[RANGE][LENGTH];
    char *pointer[RANGE];
    int count = 0;

    for (; gets(strings[count]) && count < 100 && strings[count][0] != '\0'; count++)
        pointer[count] = strings[count];

    for (int k = 0; k <= count - 1; k++) {

        for (int m = k; m <= count - 1; m++) {
            int count = strlen(pointer[k]);

            if (count > strlen(pointer[m])) {
                char *swap = pointer[k];
                pointer[k] = pointer[m];
                pointer[m] = swap;
            }
        }

        printf("%s \n", pointer[k]);
    }

    return 0;
}