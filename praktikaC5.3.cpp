#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define LENGTH 81

int divide(char string[], char *pointer[]);
void printShuffle(char *pointer[], int count);

int main()
{
    srand(time(0));
    FILE *fp;
    fp = fopen("text.txt", "r");
    char string[LENGTH];
    char *pointer[LENGTH];
    *pointer = string;

    while (fgets(string, LENGTH, fp) != NULL) {
        int count = divide(string, pointer) - 1;
        printShuffle(pointer, count);
    }

    putchar('\n');

    return 0;
}

int divide(char string[], char *pointer[])
{
    char space[] = " ";
    pointer[0] = strtok(string, space);
    int i = 1;

    for (; pointer[i-1] != NULL; ++i) {
        pointer[i] = strtok(NULL, space);
    }

        return i;
}

void printShuffle(char *pointer[], int count)
{
    int start, last;
    start = last = 0;
    for (int i = 0; i < count; ++i) {

        for ( start = 1; start < strlen(pointer[i]) - 1; ++start) {
            last = strlen(pointer[i]);
            if (last > 3) {
                while (last >= 0) {
                    last = rand() % strlen(pointer[i]);
                    if (last != 0 && last < strlen(pointer[i]) - 1)
                        break;
                }
                char ch = pointer[i][start];
                pointer[i][start] = pointer[i][last];
                pointer[i][last] = ch;
            }
        }

        printf("%s", pointer[i]);
        if (i < count - 1)
            putchar(' ');
    }

}