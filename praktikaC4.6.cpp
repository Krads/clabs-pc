
#include "stdafx.h"
#include <ctype.h>

#define LENGTH 81

int main()
{
    printf("How many relatives do u have?\n");
    int amount = 0;
    scanf("%d", &amount);
    fflush(stdin);

    printf("Enter their names and ages\n");
    char family[LENGTH][LENGTH];
    int age[LENGTH];
    int *young, *old;
    char *youngest_name, *oldest_name;
    young = old = age;
    youngest_name = oldest_name = family[0];

    for (int i = 0; i < amount; ++i) {

        printf("%d) Name: ", i+1);
        fgets(family[i], LENGTH, stdin);

        printf("Age: ");
        scanf("%d", &age[i]);
        fflush(stdin);
        if (*young > age[i]) {
            young = &age[i];
            youngest_name = family[i];
        }
        if (*old < age[i]) {
            old = &age[i];
            oldest_name = family[i];
        }

    }

    printf("The youngest relative is %d years old he's name is %s", *young, youngest_name);
    printf("The oldesest relative is %d years old he's name is %s", *old, oldest_name);

    return 0;
}
