#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>

#define SQUARE 27
#define STARS 4

void genarr(char (*arr)[SQUARE]);
void setstar(char (*arr)[SQUARE]);
void cleararr(char (*arr)[SQUARE]);

int main()
{
    srand(time(0));
    char stars[SQUARE][SQUARE];
    char (*pointstar)[SQUARE];
    pointstar = stars;

    for (int end = 0; end < SQUARE; ++end) {
        cleararr(pointstar);
        genarr(pointstar);
        setstar(pointstar);

        for (int i = 0; i < SQUARE-1; ++i) {
            printf("%s\n", pointstar[i]);
        }

        Sleep(1000);
        system("cls");
    }

    return 0;
}

// ��������� �������� ������ ��������
void genarr(char (*arr)[SQUARE])
{
    int place = 0;

    for (int i = 0; i < SQUARE/2; ++i) {
        for (int k = 0; k < STARS; ++k) {
            place = rand() % SQUARE/2;
            arr[i][place] = '*';
        }
    }

}

// ������� �������
void cleararr(char (*arr)[SQUARE])
{
    for (int i = 0; i < SQUARE; ++i) {

        for (int k = 0; k < SQUARE; ++k)
            arr[i][k] = ' ';

        arr[i][SQUARE-1] = '\0';
    }
}

// ���������� ����������� �������
void setstar(char (*arr)[SQUARE])
{
    int row = 0;
    int colon = 0;
    int start = 0;
// ������ �������
    for ( row = 0; row < SQUARE/2; ++row) {

        for (start = SQUARE/2 - 1, colon = SQUARE/2; colon < SQUARE; ++colon, --start) {
            arr[row][colon] = arr[row][start];
            arr[row][SQUARE-1] = '\0';
        }
    }
// ����� ������
    for (colon = 0; colon < SQUARE/2; ++colon) {

        for (start = SQUARE/2 - 1, row = SQUARE/2; row < SQUARE; ++row, --start) {
            arr[row][colon] = arr[start][colon];
        }
    }
// ������ ������
    for ( row = SQUARE/2; row < SQUARE; ++row) {

        for (start = SQUARE/2 - 1, colon = SQUARE/2; colon < SQUARE; ++colon, --start) {
            arr[row][colon] = arr[row][start];
        }
    }
}