#include <stdio.h>
#include <string.h>

#define LENGTH 81

int main()
{
    printf("Vvedite stroku simvolov\n");
    char string[LENGTH];
    gets(string);
    char symbol;
    int amount = 0;
    for (int i = 0; string[i] != '\0'; i++) {
        int count = 1;

        for (int k = i; (char) string[k] == (char) string[k - 1]; ++count, ++k);

        if (count > amount && string[i] != ' ') {
            symbol = string[i];
            amount = count;
        }
    }

    memset(string, symbol, amount);
    string[amount] = '\0';
    printf("V stroke: %d bykv \"%s\"\n", amount, string);

    return 0;
}