#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LENGTH 81
#define RANGE 100

int main()
{
    char strings[RANGE][LENGTH];
    char *pointer[RANGE];
    char space[] = "\n";
    int i = 0;

    FILE *fp;
    fp = fopen("start.txt", "r");

    for (; fgets(strings[i], LENGTH, fp); ++i)
        pointer[i] = strtok(strings[i], space);

    fclose(fp);
    fp = fopen("end.txt", "w");

    for (int k = 0; k <= i - 1; k++) {

        for (int m = k; m <= i - 1; m++) {
            int count = strlen(pointer[k]);

            if (count > strlen(pointer[m])) {
                char *swap = pointer[k];
                pointer[k] = pointer[m];
                pointer[m] = swap;
            }

        }
        fputs(pointer[k], fp);
        fputc('\n', fp);
    }
    fclose(fp);

    return 0;
}